set runtimepath^=/.vim runtimepath+=/.vim/after
let &packpath = &runtimepath
set nocompatible              " be iMproved, required
filetype off                  " required

" Vundle setup
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'OmniSharp/omnisharp-vim'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'tpope/vim-fugitive'
Plugin 'joshdick/onedark.vim'
Plugin 'liuchengxu/space-vim-dark'
Plugin 'tikhomirov/vim-glsl' 
Plugin 'mileszs/ack.vim'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'vim-scripts/indentpython.vim'
Plugin 'neoclide/coc.nvim', {'branch': 'release'}
Plugin 'morhetz/gruvbox'
Plugin 'prettier/vim-prettier'
Plugin 'lervag/vimtex'
Plugin 'ianks/vim-tsx'
Plugin 'sonph/onehalf', {'rtp':'vim/'} " theme
Plugin 'crusoexia/vim-monokai'
Plugin 'arcticicestudio/nord-vim'
Plugin 'ap/vim-css-color'
Plugin 'nvie/vim-flake8'
Plugin 'vim-syntastic/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'tell-k/vim-autopep8'
Plugin 'tmhedberg/SimpylFold'
Plugin 'davidhalter/jedi-vim'
Plugin 'kien/ctrlp.vim'
Plugin 'suan/vim-instant-markdown'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'dracula/vim', { 'as': 'dracula' }
Plugin 'vim-scripts/tComment'
Plugin 'vimwiki/vimwiki'
Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/utl.vim'
Plugin 'tpope/vim-repeat'
Plugin 'vim-scripts/SyntaxRange'
Plugin 'vim-scripts/HTML-AutoCloseTag'
Plugin 'honza/vim-snippets'
Plugin 'adriaanzon/vim-emmet-ultisnips'
Plugin 'SirVer/ultisnips'
Plugin 'leafgarland/typescript-vim'
call vundle#end()            " required
filetype plugin indent on    " required

" <space> leader
let mapleader = " "

" resize panes
nnoremap <silent> <Right> :vertical resize -5<cr>
nnoremap <silent> <Left> :vertical resize +5<cr>
nnoremap <silent> <Up> :resize -5<cr>
nnoremap <silent> <Down> :resize +5<cr>

" Wrap text at 100 chars.
set textwidth=100
set formatoptions=qrnt1
set wrapmargin=0
set colorcolumn=+1

" Tab = 2 spaces by default
set tabstop=2

" / search settings
set gdefault      " Never have to type /g at the end of search / replace again

" Leader mappings
nnoremap <leader>q @q

nnoremap <leader>x :x<CR>
nnoremap <leader>Xa :qa!<CR>
nnoremap <leader>X :q!<CR>

nnoremap <leader>ft :NERDTreeToggle<CR>
nnoremap <leader>fr :e ~/.config/nvim/init.vim<CR>
nnoremap <leader>fR :source ~/.config/nvim/init.vim<CR>

nnoremap <leader>re :lnext<CR>
nnoremap <leader>rE :lprevious<CR>
nnoremap <leader><Tab> :b#<CR>

nnoremap <leader>tn :tabn<CR>


nnoremap <leader>lw :VimtexCountWords<CR>
nnoremap <leader>lc :VimtexCompile<CR>

" Encoding
set encoding=utf-8

" Python specific settings
au BufNewFIle, BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

au Filetype typescript setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype typescript.tsx setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype javascriptreact setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype vimwiki setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype php setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype css setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype json setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype html setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype yaml setlocal tabstop=2 shiftwidth=2 softtabstop=0 expandtab
au Filetype cs setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au Filetype cpp setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au Filetype glsl setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab
au Filetype c setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab

" Syntax highlighting
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/
let python_highlight_all=1
syntax on

" Relative line numbering
set nu rnu

" Display commands
set showcmd


" Tmux theme settings
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Theme settings 
"
colorscheme nord
let g:airline_theme='nord'
set bg=dark

" if (has("autocmd") && !has("gui_running"))
"   augroup colorset
"     autocmd!
"     let s:white = { "gui": "#ABB2BF", "cterm": "145", "cterm16" : "7" }
"     autocmd ColorScheme * call dracula("Normal", { "fg": s:white }) " `bg` will not be styled since there is no `bg` setting
"   augroup END
" endif

" Open NERDTree and change window
" autocmd vimenter * NERDTree
" autocmd vimenter * wincmd w

set foldmethod=indent
set foldlevel=99


" Easier window navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Spelling correction settings
autocmd BufNewFile,BufRead *.md,*.txt,*.wiki,*.html,*.tex setlocal spell spelllang=en_us

autocmd BufNewFile,BufRead CMakeLists.txt setlocal nospell

" plugin settings

let g:UltiSnipsExpandTrigger="<c-o>"
let g:UltiSnipsForwardTrigger="<c-j>"
let g:UltiSnipsBackwardTrigger="<c-k>"

inoremap <silent><expr> <c-space> coc#refresh()
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"



function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction


" GoTo code navigation.
nmap <leader>gD <Plug>(coc-declaration)

nmap <leader>gd <Plug>(coc-definition)
autocmd Filetype cs nmap <leader>rr :OmniSharpGotoDefinition<CR> 

nmap <leader>gf :call <SID>show_documentation()<CR>
nmap <leader>gr <Plug>(coc-references)
nmap <leader>rf <Plug>(coc-fix-current)
nmap <leader>rF <Plug>(coc-format)

" nnoremap <leader>rr :YcmCompleter GoTo<CR>
" nnoremap <leader>rR :YcmCompleter GoToDefinition<CR>
" nnoremap <leader>rf :YcmCompleter FixIt<CR>
" nnoremap <leader>rt :YcmCompleter GetType<CR>
" nnoremap <leader>rd :YcmCompleter GetDoc<CR>
" nnoremap <leader>rs :YcmShowDetailedDiagnostic<CR>

nnoremap <silent> K :OmniSharpDocumentation<CR>


let g:syntastic_python_checkers = ['pylint']
" let g:SuperTabDefaultCompletionType = "<c-n>"
" let g:syntastic_always_populate_loc_list = 1
let g:syntastic_cs_checkers = ['code_checker']
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'    

let NERDTreeChDirMode=2
let NERDTreeIgnore=['\.vim$', '\~$', '\.pyc$', '\.swp$', '__pycache__', 'geckodriver', 'obj$']
let NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$',  '\~$']
let NERDTreeShowBookmarks=1
autocmd BufRead,BufNewFile *.ts
				\ let NERDTreeIgnore=['\.js$', '\.vim$', '\~$', '\.pyc$', '\.swp$', '__pycache__', 'geckodriver', 'obj$']

let g:vimwiki_list = [{'path': '~/Documents/vimwiki/', 'path_html': '~/Documents/vimwiki_html/'}]

let g:gruvbox_italic = 1

let g:instant_markdown_open_to_the_world = 1

let g:vimtex_view_method = 'mupdf'
autocmd FileType tex let b:coc_pairs_disabled = ['`', "'"]


let g:user_emmet_leader_key='<C-R>'
let g:user_emmet_settings = {
\  'php' : {
\    'extends' : 'html',
\    'filters' : 'c',
\  },
\  'xml' : {
\    'extends' : 'html',
\  },
\  'haml' : {
\    'extends' : 'html',
\  },
\}

let g:OmniSharp_server_stdio = 1
let g:OmniSharp_highlight_types = 3
let g:OmniSharp_lookup_metadata = 1
let g:OmniSharp_server_display_loading = 1
let g:OmniSharp_start_server = 1
let g:OmniSharp_popup = 1
let g:OmniSharp_popup_position = 'atcursor'
let g:OmniSharp_popup_mappings = { 'close': ['<Esc>', 'jk'] }
let g:omnicomplete_fetch_full_documentation = 1
let g:OmniSharp_server_use_mono = 1



set nohlsearch
hi Normal guibg=NONE ctermbg=NONE
hi LineNr ctermbg=NONE guibg=NONE

